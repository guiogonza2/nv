import matplotlib.pyplot as plt
import pandas as pd
import os
import math
import time
import easygui

os.chdir('D:\Python\datosnv')
cwd = os.getcwd()
file_name = easygui.fileopenbox()
start_time = time.time()

vehiculos = pd.read_csv(file_name)
# cambio formato columna
vehiculos['year']=vehiculos['year'].fillna(0).astype(int)
### vehiculos.to_csv(r'd:\vehiculos_year.csv')
# contar las regiones
len(pd.unique(vehiculos['region']))
# cuantos vehiculos por region hay
region = vehiculos['region'].value_counts()
#print(region)
#Eliminar filas con algun valor nulos o vacios
vehiculosn = vehiculos.dropna()
#campo county rellenar con 99999 y eliminar filas campos
vehiculos['county'] = vehiculos['county'].fillna(99999)
vehiculosn = vehiculos.dropna()
#print(vehiculosn)
### vehiculosn.to_csv(r'd:\vehiculos_n.csv')
# condiciones y precio
vehiculoscp = vehiculosn[((vehiculosn['condition'].str.contains('good')) | 
                         (vehiculosn['condition'].str.contains('excellent'))) & (vehiculosn['price'] >= 15000)]
# Marca y color
vehiculosmc = vehiculosn[(vehiculosn['manufacturer'].str.contains('chevrolet')) 
                         & (vehiculosn['paint_color'].str.contains('blue'))]
#cambio color y agregar columna para no perder el dato original
vehiculosn = vehiculos
vehiculosn['color'] = vehiculosn['paint_color'].apply(lambda x: 'blue' if x == 'black'  else 'red' if x == 'blue' else x )
#df1 para grafico inicial y grafico
df1 = vehiculosn.groupby('paint_color').count().id
vehiculosn.groupby('paint_color').count()["id"].sort_index().plot(kind='bar',width=0.9)
plt.show()
#df2 para grafico final cambio de color
df1 = vehiculosn.groupby('color').count().id
vehiculosn.groupby('color').count()["id"].sort_index().plot(kind='bar',width=0.9)
plt.show()
print("--- %s seconds ---" % (time.time() - start_time))
print("Proceso Terminado")